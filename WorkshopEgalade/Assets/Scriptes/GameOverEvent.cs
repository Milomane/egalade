﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverEvent : MonoBehaviour
{
    static private bool GameObjectExist; //l'objet est utile seulement pour savoir si on a déjà perdu sur le menu

    private void Start()
    {
        if (GameObjectExist)
        {
            Destroy(gameObject);
        } else
        {
            GameObjectExist = true;
            DontDestroyOnLoad(gameObject);
        }
    }
}
