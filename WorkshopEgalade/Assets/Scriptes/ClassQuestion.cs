﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ClassQuestion
{
    [System.Serializable]
    public class Questions 
    {
        public int goodAnswer;
        public string questionText;
        public string explication;
        public System.Collections.Generic.List<PossibleAnswers> possibleAnswers;
    }

    [System.Serializable]
    public class PossibleAnswers
    {
        public string answer;

        public PossibleAnswers(string sAnswer)
        {
            answer = sAnswer;
        }
    }
}
