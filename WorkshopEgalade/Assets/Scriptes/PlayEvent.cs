﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayEvent : MonoBehaviour
{   
    //définition de toutes les varaibles
    public float movementSpeed;
    public float stopPosition;
    public float timeAfterFadeOut;

    public string levelToLoad;

    public GameObject papof;
    public GameObject natasha;
    public GameObject fade;
    

    private Animator animatorNatasha;
    private Animator animatorPapof;
    private Transform transformNatasha;
    private Transform transformPapof;

    private bool fadingOut = false;
    private bool playActive = false;
    

    public void play() //au lancement du jeu
    {
        playActive = true; //pour dire qu'on a cliquer sur le boutons "jouer" une fois

        //définis les animators aux persos
        animatorNatasha = natasha.GetComponent<Animator>();
        animatorPapof = papof.GetComponent<Animator>();

        //définis les transform aux persos
        transformNatasha = natasha.GetComponent<Transform>();
        transformPapof = papof.GetComponent<Transform>();

        //Joue l'animation courir
        animatorNatasha.SetBool("Run", true);
        animatorPapof.SetBool("Run", true);
    }

    void Update()
    {
        if (playActive) //si on appuie sur "jouer"
        {
            if (transformPapof.position.x >= stopPosition) //déplacement des persos + stop à un certains endroit
            {
                animatorNatasha.SetBool("Run", false);
                animatorPapof.SetBool("Run", false);
                fade.gameObject.GetComponent<Animator>().SetBool("FadeOut", true);
                fadingOut = true;
            }
            else
            {
                transformPapof.position = transformPapof.position + new Vector3(movementSpeed * Time.deltaTime, 0, 0);
                transformNatasha.position = transformNatasha.position + new Vector3(movementSpeed * Time.deltaTime, 0, 0);
            }

            if (fadingOut) //Permet de changer de scène
            {
                if (timeAfterFadeOut >= -1)
                {
                    timeAfterFadeOut -= Time.deltaTime;
                } else
                {
                    Application.LoadLevel(levelToLoad);
                }
            }
        }
    }
}
