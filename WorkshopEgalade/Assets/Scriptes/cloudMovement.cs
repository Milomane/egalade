﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class cloudMovement : MonoBehaviour
{
    public float BgSpeed;
    public Renderer BgRend;

    void Update()
    {
        BgRend.material.mainTextureOffset += new Vector2(BgSpeed * Time.deltaTime, 0f); //Permet le scrolling
    }
}
