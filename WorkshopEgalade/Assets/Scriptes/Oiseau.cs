﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Oiseau : MonoBehaviour
{
    public float moveSpeed; //Permet de définir une vitesse de déplacement
    public float tpCoordonnees; //Permet de savoir les coordonnées en x où on sera plus au bonne endroit
    public Transform spawn; //La position du spawn

    void Update()
    {
        transform.position += new Vector3(-moveSpeed * Time.deltaTime, 0, 0); //Fais le déplacement

        if (transform.position.x < tpCoordonnees) //Si sa position est plus petite que celle instauré par tpCoordonnes
        {
            transform.position = spawn.position; //Ramène l'oiseau au spawn
        }
    }
}
