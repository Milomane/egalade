﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleSheetsToUnity;
using UnityEngine.SceneManagement;

public class PreChargeArray : MonoBehaviour
{
    public System.Collections.Generic.List<ClassQuestion.Questions> chargeQuestions;
    
    private static bool objectExist;

    public string excelKey;

    public RulesFadeOut fadeOut;

    public ClassQuestion.Questions CreateQuestion(System.Collections.Generic.List<GSTU_Cell> row)
    {
        var quest = new ClassQuestion.Questions();
        quest.questionText = row[0].value;
        quest.explication = row[5].value;
        quest.possibleAnswers =  new System.Collections.Generic.List<ClassQuestion.PossibleAnswers>();
        for (int i = 1; i < 5; i++)
        {
            if (row[i].value != "")
            {
                var str = row[i].value;
                if (str[0] == '!')
                {
                    quest.goodAnswer = i;
                    str = str.Remove(0, 1);
                }
                quest.possibleAnswers.Add(new ClassQuestion.PossibleAnswers(str));
            }
        }

        return quest;
    }
    
    public void Start()
    {
        excelKey = FindObjectOfType<ThemeChoice>().themeKeyString;
        
        if (!objectExist)
        {
            objectExist = true;
            DontDestroyOnLoad(transform.gameObject); // Keep the same object between scenes
        } else {
            Destroy(gameObject);
        }
        
        SpreadsheetManager.Read(new GSTU_Search(excelKey, "Feuille 1"), Test);
    }

    public void Update()
    {
        if (chargeQuestions.Count > 0)
        {
            fadeOut = FindObjectOfType<RulesFadeOut>();
            fadeOut.chargeEnd = true;
        }

        if (Application.loadedLevelName == "Menu")
        {
            Destroy(gameObject);
        }
    }
    
    
    void Test(GstuSpreadSheet spreadSheetRef)
    {
        bool firstLine = true;
        foreach (var row in spreadSheetRef.rows.primaryDictionary)
        {
            if (firstLine)
            {
                firstLine = false;
            }
            else
            {
                chargeQuestions.Add(CreateQuestion(row.Value));
            }
        }
        
        // while (spreadSheetRef[(index + "" + startLine)].value != null)
        // {
        //     //List<string> list = new List<string>(spreadSheetRef.columns);
        //     backQuestions[i].questionText = spreadSheetRef[(index + "" + startLine)].value;
        //     startLine++;
        //     i++;
        // }
    }
}
