﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RulesEvent : MonoBehaviour
{
    //initialise les variables qu'on a besoin
    public GameObject fade;
    public float timeBeforeChange;

    private bool fadeStart = false;

    void Update()
    {
        if (Input.anyKeyDown) //pour quelque soit la touche où on appuie
        {
            fade.gameObject.GetComponent<Animator>().SetBool("FadeOut", true); //Pour set le bool de l'animation FadeOut sur true
        }
    }
}
