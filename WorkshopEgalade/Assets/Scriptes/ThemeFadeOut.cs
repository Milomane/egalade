﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThemeFadeOut : MonoBehaviour
{
    public GameObject fade;
    public float timeBeforeChangeScene = 2;
    public string levelToLoad;

    private bool fadingOut = false;

    void Update()
    {
        if (fadingOut)
        {
            if (timeBeforeChangeScene >= 0)
            {
                timeBeforeChangeScene -= Time.deltaTime;
            } else
            {
                Application.LoadLevel(levelToLoad);
            }
        }
    }

    public void Fade()
    {
        fade.gameObject.GetComponent<Animator>().SetBool("FadeOut", true);
        fadingOut = true;
    }
}