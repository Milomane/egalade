﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Chrono : MonoBehaviour
{
    public Image chrono;
    public float timePassed;
    // Start is called before the first frame update
    void Start()
    {
        chrono.fillAmount = 0;
        
    }

    // Update is called once per frame
    void Update()
    {
        timePassed = timePassed + Time.deltaTime;
        chrono.fillAmount = timePassed/20;
    }
}
