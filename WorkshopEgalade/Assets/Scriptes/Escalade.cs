﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Escalade : MonoBehaviour
{
    private bool russeTurn = true;
    public Transform Papof;
    public Transform Natasha;
    private bool PapofFalling = false;
    private bool NatashaFalling = false;
    public Animator PowerPapof;
    public Animator PowerNatasha;

    public bool gameOverStart;
    public GameObject fade;
    public float timeBeforeChangeScene;
    public string levelToLoad;
    public MountainScrolling backGround;


    
    // Start is called before the first frame update
    void Start()
    {
        PowerPapof.SetBool("Help", true);
        PowerNatasha.SetBool("Help", true);
        backGround.BgSpeed = 0f;
    }

    // Update is called once per frame
    void Update()
    {
        if (gameOverStart)
        {
            timeBeforeChangeScene -= Time.deltaTime;
            if (timeBeforeChangeScene <= 0)
            {
                Application.LoadLevel(levelToLoad);
            }
        }

        //if (PowerPapof.GetBool("Hold") == false && PowerPapof.GetBool("Help") == false && PowerPapof.GetBool("Falling") == false)
        //{
        //    PowerPapof.SetBool("Climbing", true);
        //}

        //if (PowerNatasha.GetBool("Hold") == false && PowerNatasha.GetBool("Help") == false && PowerNatasha.GetBool("Falling") == false)
        //{
        //    PowerNatasha.SetBool("Climbing", true);
        //}

    }
    public void grimpe()
    {
        if (russeTurn == true)
        {
            Debug.Log("Papof TUrn");
            if (NatashaFalling)
            {
                NatashaFalling = false;
                StartCoroutine(ReTake(Natasha, Papof, PowerNatasha, new Vector2(6, Natasha.transform.position.y + 2), new Vector2(Papof.transform.position.x, Papof.transform.position.y + 1), 2f));
                //PowerPapof.SetBool("Help", true);
                PowerNatasha.SetBool("Falling", false);
                new WaitForSeconds(2f);
                StartCoroutine(Climb(PowerPapof, PowerNatasha, 2f));
                russeTurn = !russeTurn;
                StartCoroutine(ZoomCam(false));
                Debug.Log("Natasaha BackUp");
            }
            else
            {
                StartCoroutine(Climb(PowerPapof, PowerNatasha, 2f));
                Debug.Log("Papof TUrn");
            }
        }

        if (russeTurn == false)
        {
            Debug.Log("Natasha Turn");
            if (PapofFalling)
            {
                PapofFalling = false;
                StartCoroutine(ReTake(Papof, Natasha, PowerPapof, new Vector2(6, Papof.transform.position.y + 2), new Vector2(6, Natasha.transform.position.y + 1), 2f));
                //PowerNatasha.SetBool("Help", true);
                PowerPapof.SetBool("Falling", false);
                new WaitForSeconds(2f);
                StartCoroutine(Climb(PowerPapof, PowerNatasha, 2f));
                Debug.Log("Papof BackUp");
                russeTurn = !russeTurn;
                StartCoroutine(ZoomCam(false));
            }
            else
            {
                StartCoroutine(Climb(PowerPapof, PowerNatasha, 2f));
                Debug.Log("Natasha Turn");
            }

        }
        russeTurn = !russeTurn;
    }

    public void fall()
    {
        if (russeTurn == true)
        {
            if (NatashaFalling)
            {
                StartCoroutine(Fall(Papof, PowerPapof));
                PapofFalling = true;
                gameOverStart = true;
                fade.GetComponent<Animator>().SetBool("FadeOut", true);
                backGround.BgSpeed = -1f;
            }
            else
            {
                Debug.Log("Papof Fall");
                StartCoroutine(Fall(Papof, PowerPapof));
                PapofFalling = true;
                backGround.BgSpeed = 0f;
                StartCoroutine(ZoomCam(true));
            }

        }
        if (russeTurn == false)
        {
            if (PapofFalling)
            {
                StartCoroutine(Fall(Natasha, PowerNatasha));
                NatashaFalling = true;
                gameOverStart = true;
                fade.GetComponent<Animator>().SetBool("FadeOut", true);


                backGround.BgSpeed = -1f;
            }
            else
            {
                Debug.Log("Natasha Fall");
                StartCoroutine(Fall(Natasha, PowerNatasha));
                NatashaFalling = true;
                backGround.BgSpeed = 0f;
                StartCoroutine(ZoomCam(true));
            }



        }
    }
    IEnumerator Climb(Animator Papof, Animator Natasha, float timeToMove)
    {

        var currentPos = transform.position;
        var t = 0f;
        while (t < 1)
        {

            t += Time.deltaTime / timeToMove;
            backGround.BgSpeed = 0.1f;

            Papof.SetBool("Help", false);
            Natasha.SetBool("Help", false);

            Papof.SetBool("Climbing", true);
            Natasha.SetBool("Climbing", true);


            yield return null;
        }
        Papof.SetBool("Help", true);
        Natasha.SetBool("Help", true);

        Papof.SetBool("Climbing", false);
        Natasha.SetBool("Climbing", false);

        backGround.BgSpeed = 0f;

        russeTurn = !russeTurn;
    }

    IEnumerator Fall(Transform transform, Animator faller)
    {
        faller.SetBool("Climbing", false);
        faller.SetBool("Help", false);
        faller.SetBool("Falling", true);

        transform.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;

        yield return null;
        russeTurn = !russeTurn;
    }

    IEnumerator ReTake(Transform tombe, Transform rattrapeur, Animator animator, Vector2 positionTombe, Vector2 positionRattrapeur, float timeToMove)
    {


        Debug.Log("Rattrapage");
        tombe.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;
        var currentPosTombe = tombe.position;
        var currentPosRattrapeur = rattrapeur.position;
        var t = 0f;

        if (tombe.position.y <= -4)
        {
            Debug.Log(positionTombe.y);
            Debug.Log(t);
            while (t < 1)
            {
                Debug.Log(t);
                Debug.Log("pileafrire");
                t += Time.deltaTime / timeToMove;
                tombe.position = Vector2.Lerp(currentPosTombe, new Vector2(6, currentPosTombe.y + 4), t);
                rattrapeur.position = Vector2.Lerp(currentPosRattrapeur, new Vector2(6, positionRattrapeur.y + 2), t);
                yield return null;
            }
        }
        if (rattrapeur.position.y >= 2)
        {
            Debug.Log(positionTombe.y);
            Debug.Log(t);
            while (t < 1)
            {
                Debug.Log(t);
                Debug.Log("pileafrire");
                t += Time.deltaTime / timeToMove;
                tombe.position = Vector2.Lerp(currentPosTombe, new Vector2(6, positionTombe.y - 3), t);
                rattrapeur.position = Vector2.Lerp(currentPosRattrapeur, new Vector2(6, positionRattrapeur.y - 4), t);
                yield return null;
            }
        }
        else
        {
            while (t < 1)
            {
                t += Time.deltaTime / timeToMove;
                tombe.position = Vector2.Lerp(currentPosTombe, positionTombe, t);
                rattrapeur.position = Vector2.Lerp(currentPosRattrapeur, positionRattrapeur, t);
                yield return null;
            }
        }
        yield return null;
        russeTurn = !russeTurn;
    }

    IEnumerator ZoomCam(bool yesNo)
    {
        var t = 0f;
        var timeToDezoom = 4f;
        if (yesNo)
        {
            Debug.Log("Wesh");
            while (t < 1)
            {
                Debug.Log("DEZOOOOOOOOOOOOOM");
                t += Time.deltaTime / timeToDezoom;
                Camera.main.orthographicSize = Mathf.Lerp(Camera.main.orthographicSize, 7,t);
                yield return 0;
            }
        }
        else
        {
            while (t < 1)
            {
                Debug.Log("Nosh");
                t += Time.deltaTime / timeToDezoom;
                Camera.main.orthographicSize = Mathf.Lerp(Camera.main.orthographicSize, 5, t);
                yield return 0;
            }
        }
        yield return null;
    }
}