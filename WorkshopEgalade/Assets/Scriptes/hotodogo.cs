﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hotodogo : MonoBehaviour
{
    public float spawnRate;
    public GameObject hotdogo;


    void Start()
    {
        InvokeRepeating("Spawn", spawnRate, spawnRate);
    }


    void Spawn()
    {
        Instantiate(hotdogo, transform.position, Quaternion.identity);
    }
}
