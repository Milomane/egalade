﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Permet de charger la scène suivante
public class ChangeScene : MonoBehaviour
{
    public string sceneToLoad; 

    public void loadScene()
    {
        Application.LoadLevel(sceneToLoad);
    }
}
