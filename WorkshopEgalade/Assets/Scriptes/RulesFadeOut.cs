﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RulesFadeOut : MonoBehaviour
{
    public GameObject fade;
    public float timeBeforeChangeScene = 2;
    public string levelToLoad;

    private bool fadingOut = false;
    public bool chargeEnd;

    void Update()
    {
        if (Input.anyKeyDown)
        {
            fade.gameObject.GetComponent<Animator>().SetBool("FadeOut", true);
            fadingOut = true;
        }

        if (fadingOut && chargeEnd)
        {
            if (timeBeforeChangeScene >= 0)
            {
                timeBeforeChangeScene -= Time.deltaTime;
            } else
            {
                Application.LoadLevel(levelToLoad);
            }
        }
    }
}
