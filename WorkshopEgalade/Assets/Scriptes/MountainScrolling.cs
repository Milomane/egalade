﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MountainScrolling : MonoBehaviour
{
    // Définie les variables
    public float BgSpeed;
    public Renderer BgRend;

    void Update()
    {
        BgRend.material.mainTextureOffset += new Vector2(0f, BgSpeed * Time.deltaTime); //Permet le scrolling
    }
}
