﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Climbing : MonoBehaviour
{
    public Animator animator;
    private IEnumerator coroutine;

    // Start is called before the first frame update
    void Start()
    {
        animator.SetBool("Climbing", true); //La variable bool de cimbling est set à true
        coroutine = WaitAndPrint(2.0f); //Un timer qui attends 2s
        StartCoroutine(coroutine); //Permet de démarer la coroutine
    }

    private IEnumerator WaitAndPrint(float waitTime) //La boucle de la coroutine
    {
        while (true) //tant que le bool est à true
        {
            yield return new WaitForSeconds(waitTime); //attendre le temps donné par le timer
            transform.localScale = new Vector3(-transform.localScale.x, 1, 1); //Faire retourner le sprite
        }
    }
}
