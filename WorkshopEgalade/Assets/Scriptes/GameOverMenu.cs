﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverMenu : MonoBehaviour
{
    //Set les Variables dont on a besoin
    public GameObject papof;
    public GameObject natasha;
    public Transform papofSpawnPoint;
    public Transform natashaSpawnPoint;

    private GameObject gameOver;
    private bool startingGameOver;
    
    private Transform transformNatasha;
    private Transform transformPapof;

    private Animator animatorNatasha;
    private Animator animatorPapof;



    void Start()
    {
        //On récupère l'animator des persos
        animatorNatasha = natasha.GetComponent<Animator>();
        animatorPapof = papof.GetComponent<Animator>();

        //On récupère le transform des persos
        transformNatasha = natasha.GetComponent<Transform>();
        transformPapof = papof.GetComponent<Transform>();
    }

    void Update()
    {
        gameOver = GameObject.FindGameObjectWithTag("GameOver"); //on trouve l'objet avec le tag GameOver
        if (gameOver != null) //Si le GameOver n'existe pas, alors on déclanche la suite
        {
            if (!startingGameOver) //Si le GameOver n'a pas encore été activé
            {
                //on définit la position des persos au niveau de leur spawn point
                transformPapof.position = papofSpawnPoint.position; 
                transformNatasha.position = natashaSpawnPoint.position;
                startingGameOver = true; //on active le GameOver
            }
        }

        //Pour changer l'animation des persos en fonction de la hauteur
        if (transformNatasha.position.y > -4.3)
        {
            animatorNatasha.SetBool("Falling", true);
        } else
        {
            animatorNatasha.SetBool("Falling", false);
        }

        if (transformPapof.position.y > -4.3)
        {
            animatorPapof.SetBool("Falling", true);
        }
        else
        {
            animatorPapof.SetBool("Falling", false);
        }
    }
}
