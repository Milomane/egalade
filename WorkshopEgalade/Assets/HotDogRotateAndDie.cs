﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HotDogRotateAndDie : MonoBehaviour
{
    public float degrees;

    void Update()
    {
        degrees += 2;
        transform.rotation = Quaternion.Euler(Vector3.forward * degrees);
        if (transform.position.y <= -10)
        {
            Destroy(gameObject);
        }
    }
}
