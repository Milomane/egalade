﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThemeChoice : MonoBehaviour
{
    private static bool objectExist;
    public string themeKeyString;
    
    void Start()
    {
        if (!objectExist)
        {
            objectExist = true;
            DontDestroyOnLoad(transform.gameObject);
        } else {
            Destroy(gameObject);
        }
    }
    
    public void ThemeChoose(string excelKey)
    {
        themeKeyString = excelKey;
    }
}
